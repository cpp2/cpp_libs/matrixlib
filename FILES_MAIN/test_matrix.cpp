
/*! \file test_matrix.cpp
 \brief Test matrix class
 */

#include <myLib_wrapper.hpp>

#include "matrix.hpp"
#include "denseMatrix.hpp"
#include "denseMatrix2.hpp"
#include "math_toolbox.hpp"
#include "point.hpp"


/** TESTFUNCTION
 * creates a denseMatrix, fills it with some entries, some prints
 * creates 2x2 and 3x3 Matrices due to testing of computation of inverse matrices and determinant
\date 2018
\version 1.0
 */

int main(int argc, char* argv[])
{
    auto vm = myLib::parseCommandlineArguments(argc, argv);
    auto log_options = myLib::parseLoggerArguments(vm);
    myLib::initLogger(log_options);


    // Matrix Test
    denseMatrix *m = new denseMatrix(5, 3);

    LOG(output) << *m;

    m->setEntry(0, 0, 1.0);
    m->setEntry(3, 0, 2.0);
    m->setEntry(0, 1, 3.0);
    m->setEntry(2, 2, 4.0);
    m->setEntry(4, 2, 5.0);

    LOG(output) << std::endl << *m;

    // Test von denseMatrix2 fuer 2x2
    denseMatrix2 *m2 = new denseMatrix2(2) ;

    int k = 0;
    for(int i=0; i<2; i++){
        for(int j=0; j<2; j++)
        {
            m2->setEntry(i, j, (double) k) ;
            k++ ;
        }
    }

    LOG(output) << "Die Test Matrix fuer 2x2 lautet:" << std::endl << *m2;

    m2->computeDet() ;

    double detTest = m2->getDet() ;
    LOG(output) << std::endl << "Die Determinante dieser Matrix lautet: " << detTest;

    denseMatrix2 *m_inv = new denseMatrix2(2) ;
    *m_inv               = m2->getInverse() ;
    LOG(output) << std::endl << "Die Inverse dieser Matrix lautet:" << std::endl << *m_inv;

    m_inv->transpose() ;
    LOG(output) << std::endl << "Ihre Transponierte ist:" << std::endl << *m_inv;


    LOG(output) << std::endl << "Matrix mal ihre Inverse: ";
    denseMatrix2 *TEMP = new denseMatrix2(2) ;
    m_inv->transpose();

    *TEMP = *m2 * (*m_inv);

    LOG(output) << *TEMP;

    // Test von denseMatrix2 fuer 3x3
    denseMatrix2 *m3 = new denseMatrix2(3) ;

    // Leider kann man die Matrix hier nicht einfach stumpf mit aufsteigenden Zahlen fuellen..
    // A = [ 1 1 2 ; 0 0 3 ; 3 0 0 ]
    m3->setEntry(0, 0, 1) ;
    m3->setEntry(0, 1, 1) ;
    m3->setEntry(0, 2, 2) ;
    m3->setEntry(1, 2, 3) ;
    m3->setEntry(2, 0, 3) ;

    LOG(output) << std::endl << "Die Test Matrix fuer 3x3 lautet:" << std::endl << *m3;

    m3->computeDet() ;

    double detTest2 = m3->getDet() ;
    LOG(output) << std::endl << "Die Determinante dieser Matrix lautet: " << detTest2;

    denseMatrix2 *m_inv3 = new denseMatrix2(3) ;
    *m_inv3               = m3->getInverse() ;
    LOG(output) << std::endl << "Die Inverse dieser Matrix lautet:" << std::endl << *m_inv3;

    m_inv3->transpose() ;
    LOG(output) << std::endl << "Ihre Transponierte ist:" << std::endl << *m_inv3;


    LOG(output) << std::endl << "Matrix mal ihre Inverse: " ;
    denseMatrix2 *TEMP2 = new denseMatrix2(3) ;
    m_inv3->transpose();

    //    matrixMatrixMult(m3, m_inv3, TEMP2  );
    *TEMP2 = *m3 * (*m_inv3) ;

    LOG(output) << *TEMP2;

    // Matrixmultiplikation --------------------------------------------------
    LOG(output) << std::endl << "Multiplikation von Matrizen verschiedener Groesse" << std::endl << "A: " << std::endl << *m;
    LOG(output) << "B: " << std::endl << *m3;
    LOG(output) << std::endl << "A*B: " << std::endl << (*m * *m3);

    // Test fuer Matrix-Point-Multiplikation
    point *p1 = new point(1.0,2.0,1.5);
    point *p2 = new point();
    *p2 =  *m3 * *p1;

    LOG(output) << std::endl << "Point Tests: ";
    LOG(output) << "p1: " << *p1;
    LOG(output) << "p2 = B * p1 = " << *p2;

    // Operatorentest POINTS --------------------------------------------------------------


    LOG(output) << std::endl << "p1 + p2 = " << (*p1 + *p2);
    LOG(output) << std::endl << "Skalarmultiplikation: p2 * 2 = " << (*p2*2.0);
    LOG(output) << std::endl << "Skalarprodukt: p1 * p2 = " << (*p1)*(*p2);


    try{
        // Funktioniert die Exception?
        point *testEx = new point(2.0, 1.0) ;
        testEx->getXi(3);
    }
    catch (std::out_of_range &e){
        LOG(output) << std::endl << "EXCEPTION: " << e.what();
    }
    // -------------TEST FUER WENN DIE DIMENSIONEN NICHT UEBEREINSTIMMEN (KLAPPT NICHT!) --------------------

    denseMatrix2 *m4 = new denseMatrix2(2) ;
    m4->setEntry(0,0,1);
    denseMatrix2 *ERG = new denseMatrix2(3);




    // //-----------------------------------------------------------------------------
    //    // Test fuer Inneres Produkt
    //    bool testInnerProduct = false ;
    //    if (testInnerProduct){
    //        vector<double > *x = new vector<double > ;
    //        vector<double > *y = new vector<double > ;
    //
    //        x->resize(10);
    //        y->resize(10);
    //
    //        for (int i=0; i<10; i++){
    //            x->at(i) = (double) i;
    //            y->at(i) = (double) i;
    //        }
    //
    //        cout << "Eintraege von x:" << endl ;
    //        for(int i=0; i<10; i++)
    //            cout << x->at(i) << endl;
    //
    //        cout << "Eintraege von y:" << endl ;
    //        for(int i=0; i<10; i++)
    //            cout << y->at(i) << endl;
    //
    //
    //        double c = innerProduct(x, y);
    //
    //        cout << "Das Skalarprodukt lautet " << c << endl;
    //    }


    return 0;
}
