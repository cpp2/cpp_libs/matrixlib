#include "gtest/gtest.h"

#include "myLib_wrapper.hpp"
#include "matrixLib_wrapper.hpp"

double det_test()
{
    auto log_options = myLib::getStandardLoggerOptions();
    myLib::initLogger(log_options);

    // Test von denseMatrix2 fuer 2x2
    denseMatrix2 *m2 = new denseMatrix2(2) ;

    int k = 0;
    for(int i=0; i<2; i++){
        for(int j=0; j<2; j++)
        {
            m2->setEntry(i, j, (double) k) ;
            k++ ;
        }
    }

    LOG(output) << "Die Test Matrix fuer 2x2 lautet:" << std::endl << *m2;

    m2->computeDet() ;

    double detTest = m2->getDet() ;

    LOG(output) << std::endl << "Die Determinante dieser Matrix lautet: " << detTest;

    return detTest;
}

TEST(matrixlibtests, determinante)
{
    std::cout << "test" << std::endl;
    EXPECT_DOUBLE_EQ(det_test(), -2);
}
