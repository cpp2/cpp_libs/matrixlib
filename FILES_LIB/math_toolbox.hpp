/*!
 * \file toolbox.hpp
 * \brief Einige Multiplikations und Dateisystem Methoden
 */

#pragma once


#include <vector>

#include "./sparseMatrix.hpp"
#include "./point.hpp"



///Punktweise Multiplikation zweier Arrays
void pointwiseArrayMultiplication(const std::vector<double> &a, const std::vector<double> &b, std::vector<double> &c);
///Punktweise Multiplikation zweier Arrays
std::vector<double> pointwiseArrayMultiplication(const std::vector<double> &a, const std::vector<double> &b);
///Matrix Vektor Multiplikation
void matrixVectorMult(sparseMatrix &A, std::vector<double> &x, std::vector<double> &y, int n);
///Skalarprodukt für Vektoren
double innerProduct(const std::vector<double> &x, const std::vector<double> &y);
///Skalarprodukt für Punkte
double innerProduct(const point *p1, const point *p2);
///Matrix Vektor Multiplikation
void matrixVectorMult(denseMatrix &A, std::vector<double> &x, std::vector<double> &y, int n);

/// \brief Addition zweier Vektoren
std::vector<double> operator+(const std::vector<double>& v1, const std::vector<double>& v2);

/// \brief Subtraktion zweier Vektoren
std::vector<double> operator-(const std::vector<double>& v1, const std::vector<double>& v2);

/// \brief Multiplikation eines Vektors mit reellem Skalar
std::vector<double> operator*(const double &scalar, const std::vector<double> &v);


int NUM_DIG(int num);



