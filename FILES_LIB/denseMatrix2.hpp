/*!
 * \file denseMatrix2.hpp
 * \brief Matrixklasse für 'kleine' vollbesetzte Matrizen, abgeleitet von densematrix; Determinante, Inverse, etc.
 */
#pragma once

#include "./point.hpp"
#include "./denseMatrix.hpp"

/*!
 * \brief Matrixklasse für 'kleine' vollbesetzte Matrizen, abgeleitet von densematrix; Determinante, Inverse, etc.
 */

class denseMatrix2: public denseMatrix{

private:
    double det ; ///< Determinante

protected:

public:

    denseMatrix2();
    // Der Konstruktor braucht die Groesse der Matrix, da wir jetzt 2x2 und 3x3 koennen
    denseMatrix2(int size);
    ///Berechne die Determinante
    void computeDet() ;

    double getDet() const;
    ///"Hart" programmierte Inverse für 2x2 und 3x3 Matrizen
    denseMatrix2 getInverse() ;
    ///Überladen des Operators * für dense Matrizen
    denseMatrix2 operator*(const denseMatrix2 & m);
    ///* für dense Matrix und Punkt
    point operator*(const point & m);
    ///Operator stream
    friend std::ostream & operator<< (std::ostream & out, const denseMatrix2 & m);
} ;


