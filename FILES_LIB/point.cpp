#include "./point.hpp"

point::point(int dim){
    coordlist.resize(dim, 0.0);
}

point::point(double x_init) : coordlist{x_init} {};

point::point(double x_init, double y_init) : coordlist{x_init,y_init} {};

point::point(double x_init, double y_init, double z_init) : coordlist{x_init,y_init,z_init} {};

int point::getDimension() const {
    return coordlist.size() ;
}

std::vector<double> point::getPointCoord() const {
    return coordlist;
}

double point::getXi(int i) const {
    if (i > this->getDimension()){
        throw std::out_of_range("Diese Koordinate existiert in dieser Dimension nicht.");
        //        exit(EXIT_FAILURE);
    }
    else
        return coordlist[i];
    return -1;
}

double point::getX() const {
    return coordlist[0];
}

double point::getY() const {
    return coordlist[1];
}

double point::getZ() const {
    return coordlist[2];
}

void point::setPointCoord(double x_in){
    coordlist.push_back(x_in);
}

void point::setPointCoord(double x_in, double y_in){
    coordlist.push_back(x_in);
    coordlist.push_back(y_in);
}

void point::setPointCoord(double x_in, double y_in, double z_in){
    coordlist.push_back(x_in);
    coordlist.push_back(y_in);
    coordlist.push_back(z_in);
}

void point::setX(double x_in){
    if (coordlist.size() == 0){
        coordlist.resize(1);
    }
    coordlist[0] = x_in;
}

void point::setY(double y_in){
    if (coordlist.size() < 2){
        coordlist.resize(2);
    }
    coordlist[1] = y_in;
}

void point::setZ(double z_in){
    if (coordlist.size() < 3){
        coordlist.resize(3);
    }
    coordlist[2] = z_in;
}

void point::setXi(int i, double value){
    if (coordlist.size() < i+1){
        coordlist.resize(i+1);
    }
    coordlist[i] = value;
}


point point::operator+(const point &pIn)
{
    point pOut;

    for (int i = 0; i < coordlist.size(); i++){
        pOut.setXi(i, coordlist[i] + pIn.getXi(i));
    }

    return pOut;
}

void point::operator+=(const point &pIn){
    *this = *this + pIn ;
}

point point::operator-(const point &pIn)
{
    point pOut;

    for (int i = 0; i < coordlist.size(); i++){
        pOut.setXi(i, coordlist[i] - pIn.getXi(i));
    }

    return pOut;
}

double point::operator*(const point &pIn)
{
    double dOut = 0;

    for (int i = 0; i < coordlist.size(); i++){
        dOut += coordlist[i] * pIn.getXi(i);
    }

    return dOut;
}

point point::operator*(const double &a)
{
    point pOut;

    for (int i = 0; i < coordlist.size(); i++){
        pOut.setXi(i, coordlist[i] * a);
    }

    return pOut;
}

bool point::operator==(const point &pIn)
    {
    for (int i = 0; i < coordlist.size(); i++){
        if (!CLOSE(coordlist[i],pIn.getXi(i)))
            return false;
    }
    return true;
    }

bool point::operator!=(const point &pIn)
    {
    return !(this->operator ==(pIn));
    }

void point::print(){
    if (coordlist.size()==0)
        return;
    std::cout << "(";
    std::vector<double>::iterator it;
    for (it = coordlist.begin(); it != coordlist.end()-1; it++)
        std::cout << *it << ", ";
    std::cout << *it;
    std::cout << ")";
}

// Overload stream insertion operator out << c (friend)
std::ostream & operator<< (std::ostream & out, const point & p) {

    out << "(";
    int index;
    for (index = 0; index < p.getDimension()-1; index++)
        out << p.getXi(index) << ", ";
    out << p.getXi(index);
    out << ")";

    return out;
}
