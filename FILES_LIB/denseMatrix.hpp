/*!
 * \file denseMatrix.hpp
 * \brief Matrixklasse für vollbesetzte Matrizen abgeleitet von matrix; transponierbar
 */
#pragma once

#include <iostream>
#include <vector>

#include "./matrix.hpp"
#include "./sparseMatrix.hpp"
#include "./point.hpp"
#include "math_macros.hpp"



class sparseMatrix;

/*!
 \brief Matrixklasse für vollbesetzte Matrizen abgeleitet von matrix; transponierbar
 \version 1.0
 \author Lukas Düchting
 \date 2018

 */
class denseMatrix: public matrix
{
private:
    bool transposed; // Transposition an oder aus
    std::vector<double> values;

protected:

public:

    ///Konstruktor für leere denseMatrix
    denseMatrix();

    ///Erzeuge denseMatrix mit fester Anzahl Zeilen und Spalten und initialisiere mit 0
    denseMatrix(int rows, int columns);

    ///Erzeuge denseMatrix, die mit den Einträgen des C-Arrays A gefüllt wird
    denseMatrix(int rows, int columns, double ***A);

    ///Setter
    void setEntry(int row, int column, double value);

    ///Getter
    double getEntry(int row, int column) const ;

    ///Gibt den Vektor mit den Werten zurueck
    const std::vector<double> *getValues() const;
    ///Printbefehl für die Matrix
    void print();
    ///Füllt die Matrix mit einem C-Array
    void fillWithCArray(double ***, int, int);
    ///\brief Wandelt sparseMatrix in denseMatrix um
    void fillFromSparse(sparseMatrix &A);
    ///Resize für das Objekt
    void resize(int, int);
    ///Getter für die Information über Transposition
    bool isTransposed();
    ///Transposition an/ausschalten
    void setTransposed(bool tr);
    ///Indexkonvertierung von 2D auf 1D, da zB unsere dense-Matrizen als 1D Arrays gespeichert werden
    int ind2Dto1D(int row, int column) const ;
    ///Transposition an/aus
    void transpose() ;

    ///Operator Vergleich von denseMatrix
    bool operator==(const denseMatrix &m);
    ///Operator Multiplikation von denseMatrizen
    denseMatrix operator*(const denseMatrix &m);
    ///Operator Multiplikation mit point
    point operator*(const point &p);

    ///Operator stream
    friend std::ostream & operator<< (std::ostream & out, const denseMatrix & m);

} ;



