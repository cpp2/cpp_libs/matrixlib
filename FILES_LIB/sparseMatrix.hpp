#pragma once


#include <iostream>
#include <vector>
#include <stdbool.h>
#include <assert.h>

#include "./denseMatrix.hpp"
#include "./spMatrix.hpp"


class denseMatrix;
class spMatrix;


/*!
 \brief A sparse matrix class derived from matrix
 \version 1.0
 \author Lukas Duechting
 \date 2018

 */
class sparseMatrix: public matrix
{
private:
    std::vector<int> Ap;    ///< Anzahl Nichtnulleinträge in den Spalten
    std::vector<int> Ai;    ///< Zeilennummer der Nichtnulleinträge
    std::vector<double> Ax;    ///< die korrespondierenden Werte

protected:
public:

    /// Konstruktoren
    sparseMatrix();
    sparseMatrix(int row, int column);
    sparseMatrix(denseMatrix *A);

    ///Print für sparse Matrizen
    void print();

    ///resize matrix
    void resize(int rows, int columns);

    ///Fülle sparse Matrix aus dense Matrix
    void fillFromDense(denseMatrix *A);
    ///Fülle sparseMatrix aus spMatrix
    void fillFromSp(spMatrix *A);
    /// Funktioniert (noch) nicht, fuer schnelleres importieren einer spMatrix mit doppelten Eintraegen
    void fillFromSpNew(std::vector<bool> *eraseVec, spMatrix *A);
    /// \brief Erstellt sparseMatrix D = diag(A).
    sparseMatrix diag();
    /// \brief Erstellt sparseMatrix D^-1 (fuer Jacobi-Prekonditionierung)
    sparseMatrix diag_inv();
    /// \brief Extrahiert Blockdiagonale einer sparseMatrix
    std::vector<sparseMatrix> blockdiag(int blockSize);
    /// \brief F+gt Blöcke diagonal in sparseMatrix ein
    void appendBlocks(std::vector<sparseMatrix> &blocklist);
    /// \brief Gibt Anzahl der Nichtnulleinträge zurück
    int getNNZ();
    /// \brief Getter fuer Ap-Vektor
    std::vector<int>* getAp();
    /// \brief Getter fuer Ai-Vektor
    std::vector<int>* getAi();
    /// \brief Getter fuer Werte-Vektor
    std::vector<double>* getAx();

    /// \brief Gibt #nnz-Eintraege bis zur index-ten Spalte zurück
    int getApEntry(int index);
    /// \brief Gibt Zeilennr. des index-ten nnz-Eintrags zurück
    int getAiEntry(int index);
    /// \brief Gibt index-ten nnz-Eintrag zurück
    double getAxEntry(int index);

    /// \brief Setter fuer column-ten Ap-Vektor-Eintrag
    void setApEntry(int column, int numberOfEntries);
    /// \brief Haengt Zeilennr. an Ai-Vektor an
    void setAiEntry(int rowNumber);
    /// \brief Haengt nnz-Eintrag an Ax-Vektor an
    void setAxEntry(double value);

    /// \brief Mal-Operator-Ueberladung fuer sparseMatrizen
    std::vector<double> operator*(const std::vector <double> &x);

    ///Operator stream
    friend std::ostream & operator<< (std::ostream & out, const sparseMatrix & m);
};



