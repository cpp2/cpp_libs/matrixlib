#include "./spMatrix.hpp"

spMatrix::spMatrix()
: matrix()
{

}

spMatrix::spMatrix(int rows, int columns)
: matrix(rows, columns), entries(std::vector<spRep>())
{

}

spMatrix::spMatrix(int rows, int columns, denseMatrix *A)
: spMatrix(rows, columns)
{
    this->fillFromDense(rows, columns, A);
}

void spMatrix::print()
{
    std::cout << std::endl << "spMatrix size: " << entries.size() << std::endl;

    for(auto it: entries)
    {
        std::cout << "(" << it.row << ",\t" << it.column << ") \t " << it.value << std::endl;
    }
}

void spMatrix::resize(int rows, int columns)
{
    this->rows = rows;
    this->columns = columns;
    this->entries.resize(0);
}

void spMatrix::allocateEntries(int size)
{
    entries.resize(size);
}

void spMatrix::fillFromDense(int, int, denseMatrix *A)
{
    this->resize(rows, columns);
    const std::vector<double>* values = A->getValues();
    int ctr = 0;

    LOOP(column, columns){
        LOOP(row, rows){

            if (!ISZERO((*values)[A->ind2Dto1D(row, column)]))
            {
                entries.push_back(spRep(row, column, (*values)[A->ind2Dto1D(row, column)]));
                ctr++;
            }
        }
    }

}

void spMatrix::reduce()
{
    std::sort(this->entries.begin(), this->entries.end());

    std::vector<spRep> *entriesTemp = new std::vector<spRep>();

    for (std::vector<spRep>::iterator it = this->entries.begin(); it+1 != this->entries.end(); it++)
    {
        if (*it == *(it+1))
        {
            (*(it + 1)).value += (*it).value;
        }
        else
            if(!ISZERO((*it).value))
                entriesTemp->push_back(*it);
    }
    if(!ISZERO((*(this->entries.end()-1)).value))
        entriesTemp->push_back(*(this->entries.end()-1));
    entries = *entriesTemp;
    delete entriesTemp;
}

void spMatrix::append(spMatrix &m)
{
    std::vector<spRep> *entryVec = m.getEntries();
    entries.reserve(entries.size()+m.getnEntries());
    std::move(entryVec->begin(), entryVec->end(), std::inserter(entries, entries.end()));
}


int spMatrix::getnEntries()
{
    return entries.size();
}

spRep* spMatrix::getEntry(int index)
{
    return &entries[index];
}

std::vector<spRep> * spMatrix::getEntries()
{
    return &entries;
}

void spMatrix::setEntry(int index, int row, int column, double value)
{
    entries[index].row = row;
    entries[index].column = column;
    entries[index].value = value;
}

void spMatrix::insertEntry(int row, int column, double value)
{
    //TODO what is good style here

    entries.push_back(spRep(row, column, value));

    //    entries.push_back(*(new spRep(row, column, value)));

    //    spRep entry;
    //    &entry = new spRep(row, column, value);
    //    entries.push_back(entry);
}




// Overload stream insertion operator out << c (friend)
std::ostream & operator<< (std::ostream & out, const spMatrix & m)
{
    for(auto it: m.entries)
    {
        out << "(" << it.row << ",\t" << it.column << ") \t " << it.value << std::endl;
    }
    return out;
}




