/*!
 * \file math_macros.hpp
 * \brief Einige nützliche Makros (LOOP, ISZERO, PRINT, usw.)
 */
#pragma once

#include <math.h>
#include <omp.h>
#include <cassert>


///Genauigkeitsschranke für unterschiedlichste Anwendungen
const double EPS = 1.0E-13;

///Schleifenmakro
#define LOOP(i,n) for(int i = 0; i < n; i++)

///Test auf Gleichheit von doubles (bis auf Maschinengenauigkeit)
#define CLOSE(a,b) (fabs(a-b)<EPS)
///Teste auf Null
#define ISZERO(x) CLOSE(x,0.0)

///Konvertierung von 2D auf 1D Indizes
#define INDEX2TO1(row,column,columns) row*columns+column
///Print für Vektoren
#define PRINT_VECTOR(vec) for(auto it1: vec) std::cout << it1 << "\t";
///Print für Matrizen
#define PRINT_VECTORVECTOR(matrix) for(auto it2: matrix){ \
    PRINT_VECTOR(it2)\
    std::cout << std::endl;}
///Print für einen Vektor von Matrizen
#define PRINT_VECTORVECTORVECTOR(matmat) std::cout << endl; \
    for(auto it3: matmat){\
        PRINT_VECTORVECTOR(it3)\
        std::cout << std::endl;}
///for vector objects with print option
#define PRINT_VECTOROBJ(vecObj) for(auto it1: vecObj){ it1.print();\
    cout << "\t";}
#define PRINT_VECTORVECTOROBJ(vecvecobj) std::cout << endl; \
    for(auto it2: vecvecobj){\
        PRINT_VECTOROBJ(it2)\
        std::cout << std::endl;}
#define PRINT_SOLUTION(x,u) for(int i = 0; i<x.size(); i++)\
    std::cout << "x: " << x[i].getX() << "\ty: " << x[i].getY() << "\tRand: " << x[i].isDirichlet () << "\tu: " << u[i] << std::endl;

/*
#ifndef _OPENMP
#define TIMER_INIT \
    clock_t timer_start_var = clock();\
    clock_t timer_temp_var  = clock();\
    double time, time_start;
#else
#define TIMER_INIT \
    double timer_start_var = omp_get_wtime();\
    double timer_temp_var = omp_get_wtime();\
    double time, time_start;
#endif


#ifndef _OPENMP
#define CTIMER(message)              time = (double)((double)(clock() - timer_temp_var) / CLOCKS_PER_SEC);\
    time_start = (double)((double)(clock() - timer_start_var) / CLOCKS_PER_SEC);\
    std::cout << std::endl << message << time << " (total: " << time_start << ")" << std::endl;\
    timer_temp_var = clock();
#define TIMER(message, stream)              time = (double)((double)(clock() - timer_temp_var) / CLOCKS_PER_SEC);\
    time_start = (double)((double)(clock() - timer_start_var) / CLOCKS_PER_SEC);\
    stream << std::endl << message << time << " (total: " << time_start << ")";\
    timer_temp_var = clock();
#else
#define CTIMER(message)              time = (double)(omp_get_wtime() - timer_temp_var);\
    time_start = (double)(omp_get_wtime() - timer_start_var);\
    std::cout << std::endl << message << time << " (total: " << time_start << ")" << std::endl;\
    timer_temp_var = omp_get_wtime();
#define TIMER(message, stream)              time = (double)(omp_get_wtime() - timer_temp_var);\
    time_start = (double)(omp_get_wtime() - timer_start_var);\
    stream << std::endl << message << time << " (total: " << time_start << ")" << std::endl;\
    timer_temp_var = omp_get_wtime();
#endif


#ifndef _OPENMP
#define TIMER_ONLY                  time = (double)((double)(clock() - timer_temp_var) / CLOCKS_PER_SEC);\
    time_start = (double)((double)(clock() - timer_start_var) / CLOCKS_PER_SEC);\
    timer_temp_var = clock();
#else
#define TIMER_ONLY                  time = (double)(omp_get_wtime() - timer_temp_var);\
    time_start = (double)(omp_get_wtime() - timer_start_var);\
    timer_temp_var = omp_get_wtime();
#endif
*/


#define ASSERT(cond, msg)           assert(cond, msg);



