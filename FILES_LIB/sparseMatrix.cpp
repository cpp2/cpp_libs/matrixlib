#include "sparseMatrix.hpp"

sparseMatrix::sparseMatrix()
: matrix(),
  Ap(std::vector<int>(1,0)),
  Ai(std::vector<int>()),
  Ax(std::vector<double>())
{

}

sparseMatrix::sparseMatrix(int rows, int columns)
: matrix(rows, columns), Ap(std::vector<int>(columns + 1)), Ai(std::vector<int>()), Ax(std::vector<double>())
{

}

sparseMatrix::sparseMatrix(denseMatrix *A)
: sparseMatrix(A->getRows(), A->getColumns())
{
    this->fillFromDense(A);
}


void sparseMatrix::print()
{
    //    cout << endl << "Ap: " << endl;
    //    for (auto it : this->Ap)
    //        cout << it << "\t";
    //    cout << endl << "Ai: " << endl;
    //    for (auto it : this->Ai)
    //        cout << it << "\t";
    //    cout << endl << "Ax: " << endl;
    //    for (auto it : this->Ax)
    //        cout << it << "\t";

    std::cout << std::endl << std::endl << "transposed matrix: " << std::endl << std::endl;

    for(int column = 0; column<columns; column++)
    {
        int entriesInColumn = Ap[column+1]-Ap[column];
        int rowEntry = 0;

        if(entriesInColumn>0)
        {
            for(int i = 0; i < Ai[Ap[column]]; i++)
            {
                std::cout << "0\t";
                rowEntry++;
            }
            std::cout << Ax[Ap[column]] << "\t";
            rowEntry++;

            for(int index = 0; index < Ap[column+1]-Ap[column]-1; index++)
            {
                for(int i = Ai[Ap[column]+index]+1; i < Ai[Ap[column]+index+1]; i++)
                {
                    std::cout << "0\t";
                    rowEntry++;
                }
                std::cout << Ax[Ap[column]+index+1]<<"\t";
                rowEntry++;
            }
        }

        for (int i = rowEntry; i < rows; i++){
            std::cout << "0\t";
            rowEntry++;
        }

        std::cout << std::endl;
    }
}

void sparseMatrix::resize(int rows, int columns)
{
    this->rows = rows;
    this->columns = columns;
    this->Ap.assign(columns + 1, 0);
    this->Ai.resize(0);
    this->Ax.resize(0);
}


void sparseMatrix::fillFromDense(denseMatrix *A)
{
    this->resize(A->getRows(), A->getColumns());

    const std::vector<double>* values = A->getValues();
    int ctr = 0;

    LOOP(column, columns){
        LOOP(row, rows){

            if (!ISZERO((*values)[A->ind2Dto1D(row, column)]))
            {
                ctr++;
                Ax.push_back((*values)[A->ind2Dto1D(row, column)]);
                Ai.push_back(row);
            }
        }
        Ap[column + 1] = ctr;
    }

}


void sparseMatrix::fillFromSp(spMatrix *A)
{
    this->resize(A->getRows(), A->getColumns());
    A->reduce();

    LOOP(ind,A->getnEntries()){

        this->Ai.push_back(A->getEntry(ind)->row);
        this->Ax.push_back(A->getEntry(ind)->value);
    }

    int column = 0;
    LOOP(ind,A->getnEntries()){

        while (A->getEntry(ind)->column > column)
        {
            column++;
            this->Ap[column] = ind;
        }
    }
    for (int i = column + 1; i < columns + 1; i++)
        this->Ap[i] = A->getnEntries();
}


void sparseMatrix::fillFromSpNew(std::vector<bool> *eraseVec, spMatrix *A)
{
    this->resize(A->getRows(), A->getColumns());

    clock_t t_grid = clock();

    A->print();
    std::cout << std::endl;
    PRINT_VECTOR(*eraseVec)

    LOOP(ind,A->getnEntries()){
        if (!(*eraseVec)[ind])
        {
            this->Ai.push_back(A->getEntry(ind)->row);
            this->Ax.push_back(A->getEntry(ind)->value);
        }
    }

    int column = 0;
    int deletedEntries = 0;
    LOOP(ind,A->getnEntries()){

        if (!(*eraseVec)[ind])
        {
            while (A->getEntry(ind)->column > column)
            {
                column++;
                this->Ap[column] = ind-deletedEntries;
            }
        }
        else
        {
            deletedEntries++;
        }
    }
    for (int i = column + 1; i < columns + 1; i++)
        this->Ap[i] = A->getnEntries();



    double t_gridTemp = clock() - t_grid;
    double time = ((double)t_gridTemp / CLOCKS_PER_SEC);
    std::cout << std::endl << "TIME TO FILL SPARSE: " << time << std::endl;

}

sparseMatrix sparseMatrix::diag(){
    int n = this->getAp()->size() -1;

    sparseMatrix D(n,n);

    // Befuelle die Diagonale von D mit der Diagonale von A
    int counter = -1;
    int counter2 = 0;
    int numberOfEntries = 0;
    LOOP(column,n){
        //        cout << "Aktuelle Spalte: \t" << column << endl;
        numberOfEntries = this->getApEntry(column+1) - this->getApEntry(column);
        //        cout << "Anzahl an nnz Eintraegen in " << column << "-ter Spalte: \t" << numberOfEntries << endl;
        if ( numberOfEntries > 0){
            LOOP(entry, numberOfEntries) {

                counter ++;
                //                cout << "Zeilennummer des " << counter << "-ten nnz Eintrags (in der " << column << "-ten Spalte): \t" << this->getAiEntry(counter) << endl;
                if (this->getAiEntry(counter) == column )
                {
                    //                    cout << endl << "Diagonale, also setze den Wert " << this->getAxEntry(counter) << " in D ein." << endl;
                    D.setAxEntry(this->getAxEntry(counter));
                    //                    cout << endl << "... in der " << this->getAiEntry(counter) <<"-ten Zeile und " << column <<"-ten Spalte." << endl;
                    D.setAiEntry(this->getAiEntry(counter));

                    counter2++;

                    //                    break;


                }

                //                cout << "Counter: \t" << counter << endl << "Counter2: \t" << counter2 << endl;

            }

        }
        D.setApEntry(column+1,counter2);
    }

    //    cout << "Nichtnullteintrag-Liste von D:" << endl;
    //
    //    PRINT_VECTOR(D.Ax);
    //    cout << endl << "Ai von D: " << endl;
    //    PRINT_VECTOR(D.Ai);
    //    cout << endl << "Ap von D:" << endl;
    //    PRINT_VECTOR(D.Ap);
    //    cout << endl;

    return D;
}

sparseMatrix sparseMatrix::diag_inv(){
    int n = this->getAp()->size() -1;

    sparseMatrix D(n,n);

    // Befuelle die Diagonale von D mit der Diagonale von A
    int counter = -1;
    int counter2 = 0;
    int numberOfEntries = 0;
    LOOP(column,n){
        // cout << "Aktuelle Spalte: \t" << column << endl;
        numberOfEntries = this->getApEntry(column+1) - this->getApEntry(column);
        // cout << "Anzahl an nnz Eintraegen in " << column << "-ter Spalte: \t" << numberOfEntries << endl;
        if ( numberOfEntries > 0){
            LOOP(entry, numberOfEntries) {

                counter ++;
                // cout << "Zeilennummer des " << counter << "-ten nnz Eintrags (in der " << column << "-ten Spalte): \t" << this->getAiEntry(counter) << endl;
                if (this->getAiEntry(counter) == column )
                {
                    // cout << endl << "Diagonale, also setze den Wert " << this->getAxEntry(counter) << " in D ein." << endl;
                    D.setAxEntry(1 / (this->getAxEntry(counter)) );
                    // cout << endl << "... in der " << this->getAiEntry(counter) <<"-ten Zeile und " << column <<"-ten Spalte." << endl;
                    D.setAiEntry(this->getAiEntry(counter));

                    counter2++;

                }

                // cout << "Counter: \t" << counter << endl << "Counter2: \t" << counter2 << endl;

            }

        }
        D.setApEntry(column+1,counter2);
    }

    // cout << "Nichtnullteintrag-Liste von D:" << endl;
    //
    // PRINT_VECTOR(D.Ax);
    // cout << endl << "Ai von D: " << endl;
    // PRINT_VECTOR(D.Ai);
    // cout << endl << "Ap von D:" << endl;
    // PRINT_VECTOR(D.Ap);
    // cout << endl;

    return D;
}

std::vector<sparseMatrix> sparseMatrix::blockdiag(int blockSize){

    // Anzahl Bloecke
    int numBlocks;
    // Groesse des letzten Blocks, falls kleiner als die restlichen
    int blockSize2 = 0;
    int rows = this->getRows();
    int columns = this->getColumns();

    // Checke, ob Matrix quadratisch ist
    assert(rows == columns);

    // Bestimme die Anzahl der Bloecke.
    double numBlocksTemp = (double)rows / (double)blockSize;
    std::cout << "Anzahl Bloecke der Groesse " << blockSize << " :  \t" << numBlocksTemp << std::endl;

    if (rows % blockSize > 0){
        // Falls Rest uebrig bleibt, nehmen wir einen kleineren zusaetzlichen Block hinzu.
        numBlocks = (int)numBlocksTemp + 1;
        // Bestimme Groesse des letzten, kleineren Blockes
        blockSize2 = rows % blockSize;
        std::cout << "Groesse des kleineren Blocks: \t" << blockSize2 << std::endl;
        // Allokiere zusatzliche sparseMatrix vor
        sparseMatrix lastBlock(blockSize2,blockSize2);
    }
    else {
        numBlocks = numBlocksTemp;
    }

    std::cout << "Finale Anzahl Bloecke: \t" << numBlocks << std::endl;


    std::vector<sparseMatrix> blocklist(numBlocks);
    sparseMatrix block;

    // Anzahl nnz-Eintraege einer bzw. mehrerer Spalten
    int numberOfEntriesColumn, numberOfEntriesColumns;
    int counter = 0;
    int counter2;
    int correction = 0;

    //#pragma omp parallel for shared(numBlocks,blockSize2,blocklist) private(blockSize,block,correction,counter2,numberOfEntriesColumn,numberOfEntriesColumns) reduction(+:counter)
    // Schleife ueber die Bloecke
    LOOP(blockNumber,numBlocks){

        //        cout << endl << "Aktueller Block: " << blockNumber+1 << " von " << numBlocks << endl;
        counter2 = 0;

        // Leere den Block
        block.resize(blockSize,blockSize);

        // Falls der letzte Block kleiner ist als die anderen...
        if (blockSize2 > 0 && blockNumber == numBlocks - 1){
            //            blockSize = blockSize2;
            block.resize(blockSize2,blockSize2);
            correction = blockSize - blockSize2;
        }

        // Bestimme Anzahl nnz-Eintraege in den Spalten des aktuellen Blocks
        numberOfEntriesColumns = this->getApEntry( ((blockNumber+1) * blockSize) - correction) - this->getApEntry(blockNumber * blockSize);
        // cout << endl << "Anzahl nnz-Eintraege im " << blockNumber+1 << "-ten Block (und darunter): \t" << numberOfEntriesColumns << endl;


        if (numberOfEntriesColumns > 0){
            counter2 = 0;
            for(int column = blockNumber * blockSize; column < (blockNumber+1) * blockSize - correction; column++){


                // cout << endl << "Aktuelle Spalte: \t" << column+1 << endl;

                // Anzahl nnz-Eintraege in aktueller Spalte
                numberOfEntriesColumn = this->getApEntry(column+1) - this->getApEntry(column);
                //                 cout << "\tAnzahl nnz-Eintraege in der " << column+1 << "-ten Spalte: \t" << numberOfEntriesColumn << endl;

                if (numberOfEntriesColumn > 0){

                    counter = this->getApEntry(column);

                    while (counter < this->getApEntry(column+1)){

                        counter++;

                        if (this->getAiEntry(counter-1) > (blockNumber+1) * blockSize - correction) continue;

                        // cout << "\t \tcounter: \t" << counter << endl;
                        // cout <<"\t \t" << counter <<"-ter nnz-Eintrag befindet sich in der " << this->getAiEntry(counter-1) << "-ten Zeile." << endl;

                        if (this->getAiEntry(counter-1) < (blockNumber+1) * blockSize - correction && this->getAiEntry(counter-1) >= blockNumber * blockSize)
                        {
                            // cout << "\t \t \tnnz-Eintrag innerhalb des " << blockNumber << "-ten Blocks gefunden." << endl;
                            // cout << endl << "Diagonale, also setze den Wert " << this->getAxEntry(counter) << " in D ein." << endl;
                            block.setAxEntry( this->getAxEntry(counter-1) );
                            // cout << endl << "... in der " << this->getAiEntry(counter) <<"-ten Zeile und " << column <<"-ten Spalte." << endl;
                            block.setAiEntry( this->getAiEntry(counter-1) - (blockNumber * blockSize) );
                            counter2++;
                        }
                    }

                }
                block.setApEntry(column+1 - (blockNumber * blockSize),counter2);
            }

        }

        blocklist[blockNumber] = block;

    }

    return blocklist;

}

void sparseMatrix::appendBlocks(std::vector<sparseMatrix> &blocklist){

    std::vector<double> *AxBlock;
    std::vector<int> *AiBlock;
    std::vector<int> *ApBlock;
    int blockSize = blocklist[0].getRows();
    std::vector<int> *ApBlockPrev = new std::vector<int>(blockSize+1,0);

    LOOP(block,blocklist.size()){

        //        cout << block+1 << "-ter Block: " << endl;
        //        blocklist[block].print();

        AxBlock = blocklist[block].getAx();
        std::move(AxBlock->begin(), AxBlock->end(), std::inserter(Ax, Ax.end()));

        AiBlock = blocklist[block].getAi();

        // Verschiebe die Zeilen
        LOOP(i,blocklist[block].getNNZ()){
            //            AiBlock->push_back(AiBlockTemp->at(i) + blockSize);
            AiBlock->at(i) += block * blockSize;
        }

        //        cout << "AiBlock(" << block+1 << "): \t" << endl;
        //        PRINT_VECTOR(*AiBlock);
        //        cout << endl;

        ApBlock = blocklist[block].getAp();

        // Passe die Ap-Eintraege an
        LOOP(j,blocklist[block].getRows() + 1){
            ApBlock->at(j) += ApBlockPrev->at(blockSize);
            //            cout << endl << "ApBlockPrev[end] = " << ApBlockPrev->at(blockSize) << endl;
        }

        //        cout << endl << "ApBlock(" << block+1 << "): \t" << endl;
        //        PRINT_VECTOR(*ApBlock);
        //        cout << endl;

        std::move(AiBlock->begin(), AiBlock->end(), std::inserter(Ai, Ai.end()));
        std::move(ApBlock->begin()+1, ApBlock->end(), std::inserter(Ap, Ap.end()));

        blockSize = blocklist[block].getRows();
        this->rows += blockSize;
        this->columns += blockSize;

        ApBlockPrev = ApBlock;

    }


}

int sparseMatrix::getNNZ(){
    return this->getAx()->size();
}

std::vector<int>* sparseMatrix::getAi()
{
    return &Ai;
}

std::vector<int>* sparseMatrix::getAp()
{
    return &Ap;
}

std::vector<double>* sparseMatrix::getAx()
{
    return &Ax;
}


int sparseMatrix::getAiEntry(int index)
{
    return Ai[index];
}

int sparseMatrix::getApEntry(int index)
{
    return Ap[index];
}

double sparseMatrix::getAxEntry(int index)
{
    return Ax[index];
}

void sparseMatrix::setApEntry(int column, int numberOfEntries)
{
    this->Ap[column] = numberOfEntries;
}

void sparseMatrix::setAiEntry(int rowNumber)
{
    this->Ai.push_back(rowNumber);
}

void sparseMatrix::setAxEntry(double value)
{
    this->Ax.push_back(value);
}



std::vector<double> sparseMatrix::operator*(const std::vector <double> &x)
{
    std::vector<double> y(x.size(), 0.0);
    int cEntry;

    for(int column = 1; column<columns+1; column++)
    {
#pragma omp parallel for shared(x,y) private(cEntry)
        for(cEntry = getApEntry(column-1); cEntry<getApEntry(column); cEntry++)
        {
            y[getAiEntry(cEntry)] += x[column - 1] * getAxEntry(cEntry);
        }
    }

    return y;
}


// Overload stream insertion operator out << c (friend)
std::ostream & operator<< (std::ostream & out, const sparseMatrix & m)
{
    for(int column = 0; column<m.columns; column++)
    {
        int entriesInColumn = m.Ap[column+1]-m.Ap[column];
        int rowEntry = 0;

        if(entriesInColumn>0)
        {
            for(int i = 0; i < m.Ai[m.Ap[column]]; i++)
            {
                out << "0\t";
                rowEntry++;
            }
            out << m.Ax[m.Ap[column]] << "\t";
            rowEntry++;

            for(int index = 0; index < m.Ap[column+1]-m.Ap[column]-1; index++)
            {
                for(int i = m.Ai[m.Ap[column]+index]+1; i < m.Ai[m.Ap[column]+index+1]; i++)
                {
                    out << "0\t";
                    rowEntry++;
                }
                out << m.Ax[m.Ap[column]+index+1]<<"\t";
                rowEntry++;
            }
        }

        for (int i = rowEntry; i < m.rows; i++){
            out << "0\t";
            rowEntry++;
        }

        out << std::endl;
    }
    out << "(transposed)";
    return out;
}




