#pragma once

#include "denseMatrix.hpp"
#include "denseMatrix2.hpp"
#include "sparseMatrix.hpp"
#include "math_toolbox.hpp"
#include "point.hpp"
#include "math_macros.hpp"
#include "spMatrix.hpp"
#include "matrix.hpp"
