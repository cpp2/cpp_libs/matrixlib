#pragma once

#include <iostream>
#include <vector>
#include <algorithm>

#include "./denseMatrix.hpp"


class denseMatrix;


/// \brief sp-Matrixformat-Eintragsrepräsentation; Jeder Eintrag hat Zeilenindex, Spaltenindex und nichtnull-Wert
struct spRep
{
    int row;
    int column;
    double value;
    spRep()
    : row(0), column(0), value(0)
    {
    }
    spRep(int row, int column, double value)
    : row(row), column(column), value(value)
    {
    }

    bool operator<(const spRep& a) const
    {
        if (column == a.column) return row < a.row;
        return column < a.column;
    }
    bool operator==(const spRep& a) const
        {
        if (column == a.column && row == a.row) return true;
        return false;
        }
};

/*!
 \brief Ein Matrixformat, abgeleitet von matrix. Hat eine volle Repräsentation jedes nicht-null Eintrags. 'doppelte' Einträge sind erlaubt.
 A sparse matrix class derived from matrix, this is a full representation with row and column indizes and nonzero value entry
 \version 1.0
 \author Lukas Duechting
 \date 2018

 */
class spMatrix: public matrix
{
private:
    std::vector<spRep> entries;

protected:
public:
    spMatrix();
    spMatrix(int rows, int columns);
    spMatrix(int rows, int columns, denseMatrix *A);

    void print();

    /// Setzt eine Nullmatrix der angegebenen Größe
    void resize(int rows, int columns);

    /// resized den entries-vektor
    void allocateEntries(int size);

    /// Füllt die sp-Matrix aus dem dense-Format
    void fillFromDense(int rows, int columns, denseMatrix *A);


    /// Sortiert die Eintraege und fuehrt doppelte zusammen. Vorbereitung um dann von einer sparse-Matrix 'fillFromSP' aufzurufen.
    void reduce();

    /// Hängt eine andere spMatrix an
    void append(spMatrix &m);


    int getnEntries();
    spRep* getEntry(int index);
    std::vector<spRep> * getEntries();
    void setEntry(int index, int row, int column, double value);
    void insertEntry(int row, int column, double value);

    ///Operator stream
    friend std::ostream & operator<< (std::ostream & out, const spMatrix & m);
};




