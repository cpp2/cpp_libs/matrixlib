#include "./denseMatrix2.hpp"


denseMatrix2::denseMatrix2()
:denseMatrix(), det(0.0)
{}

denseMatrix2::denseMatrix2(int size)
:denseMatrix(size, size), det(0.0)
{}


double denseMatrix2::getDet() const {
    return det ;
}

void denseMatrix2::computeDet()
{
    if (this->rows==1)
    {
        // Setze Determinante
        this->det = getEntry(0,0) ;
    }
    else if (this->rows==2){
        /// Hole Werte
        double a11, a12, a21, a22 ;

        a11 = this->getEntry(0,0) ;
        a21 = this->getEntry(1,0) ;
        a12 = this->getEntry(0,1) ;
        a22 = this->getEntry(1,1) ;

        // Setze Determinante
        this->det = (a11 * a22) - (a21 * a12) ;
    }
    else if (this->rows==3){
        // Hole Werte
        double a11, a12, a13, a21, a22, a23, a31, a32, a33 ;
        // Erste Zeile
        a11 = this->getEntry(0,0) ;
        a12 = this->getEntry(0,1) ;
        a13 = this->getEntry(0,2) ;
        // Zweite Zeile
        a21 = this->getEntry(1,0) ;
        a22 = this->getEntry(1,1) ;
        a23 = this->getEntry(1,2) ;
        // Dritte Zeile
        a31 = this->getEntry(2,0) ;
        a32 = this->getEntry(2,1) ;
        a33 = this->getEntry(2,2) ;

        // Setze Determinante mit Regel von Sarrus
        //        this->det = a11*a22*a33 + a12*a23*a31 + a13*a21*a32
        //            - a31*a22*a13 -a32*a23*a11 - a33*a21*a12 ;
        this->det = a11*a22*a33 - a11*a23*a32 - a12*a21*a33
            + a12*a23*a31 + a13*a21*a32 - a13*a22*a31 ;
    }
}

denseMatrix2 denseMatrix2::getInverse() {

    this->computeDet() ;

    denseMatrix2 matrix_invers(this->rows ) ;

    if ( ISZERO( det ) ){
        std::cerr << "exception: " << std::invalid_argument ("Die zu invertierende Matrix ist singulaer.").what() << std::endl;
        exit(EXIT_FAILURE);
    }
    if (this->rows == 1)
    {
        // Setze inverse Matrix
        matrix_invers.setEntry(0, 0, 1.0/det ) ;
    }

    else if (this->rows == 2){
        double a11, a21, a12, a22 ;
        a11 = this->getEntry(0,0) ;
        a21 = this->getEntry(1,0) ;
        a12 = this->getEntry(0,1) ;
        a22 = this->getEntry(1,1) ;
        // Setze inverse Matrix
        matrix_invers.setEntry(0, 0, a22/ det ) ;
        matrix_invers.setEntry(0, 1, -a12/ det ) ;
        matrix_invers.setEntry(1, 0, -a21/ det ) ;
        matrix_invers.setEntry(1, 1, a11/ det ) ;
    }
    else if (this->rows == 3){
        // Hole Werte
        double a11, a12, a13, a21, a22, a23, a31, a32, a33 ;
        double b11, b12, b13, b21, b22, b23, b31, b32, b33 ;
        // Erste Zeile
        a11 = this->getEntry(0,0) ;
        a12 = this->getEntry(0,1) ;
        a13 = this->getEntry(0,2) ;
        // Zweite Zeile
        a21 = this->getEntry(1,0) ;
        a22 = this->getEntry(1,1) ;
        a23 = this->getEntry(1,2) ;
        // Dritte Zeile
        a31 = this->getEntry(2,0) ;
        a32 = this->getEntry(2,1) ;
        a33 = this->getEntry(2,2) ;
        // Setze inverse Matrix
        double factor ;
        factor = 1/det ;
        b11 = factor * (a22*a33 - a23*a32) ;
        b12 = factor * (a13*a32 - a12*a33) ;
        b13 = factor * (a12*a23 - a13*a22) ;

        b21 = factor * (a23*a31 - a21*a33) ;
        b22 = factor * (a11*a33 - a13*a31) ;
        b23 = factor * (a13*a21 - a11*a23) ;

        b31 = factor * (a21*a32 - a22*a31) ;
        b32 = factor * (a12*a31 - a11*a32) ;
        b33 = factor * (a11*a22 - a12*a21) ;

        matrix_invers.setEntry(0,0, b11) ;
        matrix_invers.setEntry(0,1, b12) ;
        matrix_invers.setEntry(0,2, b13) ;

        matrix_invers.setEntry(1,0, b21) ;
        matrix_invers.setEntry(1,1, b22) ;
        matrix_invers.setEntry(1,2, b23) ;

        matrix_invers.setEntry(2,0, b31) ;
        matrix_invers.setEntry(2,1, b32) ;
        matrix_invers.setEntry(2,2, b33) ;
    }
    else
    {
        std::cerr << "exception: " << std::domain_error ("Die zu invertierende Matrix muss 2x2 oder 3x3 sein.").what() << std::endl;
        exit(EXIT_FAILURE);
    }

    return matrix_invers ;
}



denseMatrix2 denseMatrix2::operator* (const denseMatrix2 &A){

    int rows = A.getRows() ;

    if (rows != this->getRows() ){

        std::cerr << "exception: " << std::domain_error ("Die Matrizen muessen die gleiche Groesse haben!").what() << std::endl;
        exit(EXIT_FAILURE);
    }
    double entry ;
    denseMatrix2 tmp(rows ) ;
    //Laufe durch alle Einträge
    LOOP(row, rows ){
        LOOP(column, rows ){
            entry = 0 ;
            // Berechne den jeweiligen Eintrag
            LOOP(i, rows ){
                entry += this->getEntry(row, i) * A.getEntry(i, column) ;
            }
            tmp.setEntry(row, column, entry ) ;
        }
    }
    return tmp ;
}

point denseMatrix2::operator* (const point &p_in){
    int size = this->getRows() ;
    if (size != p_in.getDimension() ){
        std::cerr << "exception: " << std::domain_error("Matrix und Point passen nicht zusammen!").what() << std::endl;
        exit(EXIT_FAILURE);
    }
    point p(size) ;
    double entry ;
    //Laufe durch alle Einträge
    LOOP(row, size){
        entry = 0 ;
        //Berechne den jeweiligen Eintrag
        LOOP(i, size){

            entry += this->getEntry(row, i) * p_in.getPointCoord()[i] ;
        }
        p.setXi(row,entry);
    }
    return p ;
}




// Overload stream insertion operator out << c (friend)
std::ostream & operator<< (std::ostream & out, const denseMatrix2 & m)
{
    LOOP(row,m.rows){
        LOOP(column, m.columns){

            out << m.getEntry(row, column) << "\t";
        }

        if(row < m.rows-1) out << std::endl;
    }
    return out;
}


