
#include "./matrix.hpp"

matrix::matrix()
: rows(0), columns(0)
{
}

matrix::matrix(int rows_in, int columns_in)
: rows(rows_in), columns(columns_in)
{
}

int matrix::getRows() const
{
    return this->rows;
}

int matrix::getColumns() const
{
    return this->columns;
}


