
#include "./math_toolbox.hpp"

void pointwiseArrayMultiplication(const std::vector<double> &a, const std::vector<double> &b, std::vector<double> &c)
{
    LOOP(i,a.size()){

        c[i] = a[i] * b[i];
    }
}

std::vector<double> pointwiseArrayMultiplication(const std::vector<double> &a, const std::vector<double> &b)
{
    std::vector<double> c;
    c.resize(a.size());

    LOOP(i,a.size()){

        c[i] = a[i] * b[i];
    }

    return c;
}

void matrixVectorMult(sparseMatrix &A, std::vector<double> &x, std::vector<double> &y, int n)
{
    fill(y.begin(), y.end(), 0.0);

    int cEntry;

    for(int column = 1; column<A.getColumns()+1; column++)
    {
#pragma omp parallel for shared(A,x,y,n,column) private(cEntry)
        for(cEntry = A.getApEntry(column-1); cEntry<A.getApEntry(column); cEntry++)
        {
            y[A.getAiEntry(cEntry)] += x[column - 1] * A.getAxEntry(cEntry);
        }
    }
}

//void pointwiseArrayMultiplication(vector<double> *a, vector<double> *b, vector<double> *c)
//{
//    LOOP(i,a->size()){
//
//        (*c)[i] = ((*a)[i] * (*b)[i]);
//    }
//}
//
//
double innerProduct(const std::vector<double> &x, const std::vector<double> &y)
{
    double result = 0.0 ;
    int length = x.size();
    int i;
#pragma omp parallel for shared(x,y,length) private (i) reduction(+:result)

    for(i = 0; i < length; i++){
        result += x[i] * y[i] ;
    }

    return result ;
}
//
double innerProduct(const point *p1, const point *p2)
{
    double tmp = 0.0 ;
    double x1, x2, y1, y2 ;
    x1 = p1->getX() ;
    x2 = p2->getX() ;
    y1 = p1->getY() ;
    y2 = p2->getY() ;

    tmp = x1*x2 + y1*y2 ;
    return tmp ;
}
//
//
void matrixVectorMult(denseMatrix &A, std::vector<double> &x, std::vector<double> &y, int n)
{
    const std::vector<double>* values = A.getValues();
    int row;
    double entry;

#pragma omp parallel for shared(A,x,y,n) private(row, entry)
    for (row = 0; row < n; row++)
    {
        entry = 0.0;
        for (int col = 0; col < n; col++)
        {
            entry += (*values)[A.ind2Dto1D(row,col)] * x[col];
        }

        y[row] = entry;
    }
}
//
//
//void dense2pointMult(denseMatrix2 *A, const point *x, point *y)
//{
//    vector<double>* values = A->getValues();
//
//    y->setX( (*values)[A->ind2Dto1D(0, 0)]*x->getX() + (*values)[A->ind2Dto1D(0, 1)]*x->getY() );
//    y->setY( (*values)[A->ind2Dto1D(1, 0)]*x->getX() + (*values)[A->ind2Dto1D(1, 1)]*x->getY() );
//}
//
//
//void matrixMatrixMult(denseMatrix *A, denseMatrix *B, denseMatrix *OUT)
//{
//    OUT->resize(A->getRows(), A->getColumns());
//
//    for (int rowOut = 0; rowOut < A->getRows(); rowOut++)
//    {
//        for (int colOut = 0; colOut < A->getColumns(); colOut++)
//        {
//            for (int entry = 0; entry < A->getColumns(); entry++)
//            {
//                (*OUT->getValues())[OUT->ind2Dto1D(rowOut, colOut)] += A->getEntry(rowOut, entry) * B->getEntry(entry, colOut);
//            }
//        }
//    }
//}

std::vector<double> operator+(const std::vector<double>& v1, const std::vector<double>& v2){

    int rows = v1.size();

    assert(rows==v2.size());

    std::vector<double> result;
    result.resize(rows);

#pragma omp parallel for shared(rows,v1,v2,result)
    LOOP(i,rows){
        result[i] = v1[i] + v2[i];
    }

    return result;
}

std::vector<double> operator-(const std::vector<double>& v1, const std::vector<double>& v2){


    int rows = v1.size();

    assert(rows==v2.size());

    std::vector<double> result;
    result.resize(rows);

#pragma omp parallel for shared(rows,v1,v2,result)
    LOOP(i,rows){
        result[i] = v1[i] - v2[i];
    }

    return result;
}

std::vector<double> operator*(const double &scalar, const std::vector<double> &v){

    int rows = v.size();
    std::vector<double> result;
    result.resize(rows);

#pragma omp parallel for shared(scalar,v,rows,result)
    LOOP(i,rows){
        result[i] = scalar * v[i];
    }

    return result;

}



int NUM_DIG(int num)
{
    int stellen = 0;
    while(num > 0)
    {
        num /= 10;
        ++stellen;
    }
    return stellen;
}

