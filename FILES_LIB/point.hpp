/*!
 * \file point.hpp
 * \brief Eine Basis-Punktklasse mit Koordinaten
 */
#pragma once

#include <iostream>
#include <vector>

#include "math_macros.hpp"




/*!
 * Klasse Punkte beliebiger Dimension.
 */
class point{
private:

protected:
    std::vector<double> coordlist ; ///< Vektor mit Koordinaten (beliebige Dimension)
    //    int dimension;

public:
    /// \brief Konstruktor fuer beliebige Dimension
    point(int dim);
    /// \brief Konstruktor 1D
    point(double x_init);
    /// \brief Konstruktor 2D
    point(double x_init, double y_init);
    /// \brief Konstruktor 3D
    point(double x_init, double y_init, double z_init);

    ///Standardkonstruktor für "leeren" Punkt
    point() {};

    /// \brief Gibt Dimension zurueck
    int getDimension() const;

    /// \brief Getter fuer Koordinaten eines Punktes
    std::vector<double> getPointCoord() const;

    /// \brief Getter fuer einzelne Koordinate
    double getXi(int i) const;

    /// \brief Getter fuer x-,y- bzw. z-Koordinate
    // TODO Spaeter ggf. loeschen und nur getPointCoord benutzen
    double getX() const;
    double getY() const;
    double getZ() const;

    /// \brief Setter fuer Koordinaten (fuer Dimension 1 bis 3)
    void setPointCoord(double x_in);

    void setPointCoord(double x_in, double y_in);

    void setPointCoord(double x_in, double y_in, double z_in);

    //Setters für die Attribute
    //TODO Spaeter gegebenfalls loeschen und nur setPointCoord benutzen
    void setX(double x_in);
    void setY(double y_in);
    void setZ(double z_in);

    /// \brief Setzt Koordinate x_i auf value
    void setXi(int i, double value);


    /// \brief Überlade den Additionsoperator für Points
    point operator+(const point &pIn);
    /// \brief Überlade den Additionsoperator für Points
    void operator+=(const point &pIn);
    /// \brief Überlade den Subtraktionsoperator für Points
    point operator-(const point &pIn);
    /// \brief Überlade Skalarmultiplikation
    point operator*(const double &a);
    /// \brief Überlade Skalarprodukt (Vorsicht!)
    double operator*(const point &pIn);
    /// \brief Vergleicht zwei Punkte auf Gleichheit
    bool operator==(const point &pIn);
    /// \brief Vergleicht zwei Punkte auf Ungleichheit
    bool operator!=(const point &pIn);

    /*! \brief Print-Funktion fuer Points.
     * Gibt alle Koordinaten eines Punktes aus.
     */
    void print();

    friend std::ostream & operator<< (std::ostream & out, const point & p);

} ;




