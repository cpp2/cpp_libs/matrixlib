#include "./denseMatrix.hpp"

denseMatrix::denseMatrix()
: matrix(), transposed(false), values(std::vector<double>())
{

}

denseMatrix::denseMatrix(int rows, int columns)
: matrix(rows, columns), transposed(false), values(std::vector<double>(rows * columns, 0.0))
{

}

denseMatrix::denseMatrix(int rows, int columns, double ***A)
: matrix(rows, columns), transposed(false), values(std::vector<double>(rows * columns))
{
    this->fillWithCArray(A, rows, columns);
}


void denseMatrix::setEntry(int row, int column, double value)
{
    values[ind2Dto1D(row,column)] = value;
}

double denseMatrix::getEntry(int row, int column) const
{
    return values.at(ind2Dto1D(row,column) ) ;
}

const std::vector<double>* denseMatrix::getValues() const{
    return &values;
}

void denseMatrix::print()
{
    LOOP(row,rows){
        LOOP(column, columns){

            std::cout << values[ind2Dto1D(row, column)] << "\t";
        }

        std::cout << std::endl;
    }
}

void denseMatrix::fillWithCArray(double ***A, int rows, int columns)
{
    this->resize(rows, columns);
    if (rows == this->rows && columns == this->columns)
    {
        LOOP(row,rows){
            LOOP(column,columns){

                this->values[ind2Dto1D(row, column)] = (*A)[row][column];
            }
        }
    }
    else
    {
        std::cerr << "Zeilen-/Spaltenlängen stimmen beim befüllen einer Matrix nicht überein. " << std::endl;
    }
}

void denseMatrix::fillFromSparse(sparseMatrix &A){

    int rows = A.getRows();
    int columns = A.getColumns();
    int row;
    double entry;

    this->resize(rows,columns);

    LOOP(column,columns){
//        numberOfEntries = A.getApEntry(column+1);
        for (int counter = A.getApEntry(column); counter < A.getApEntry(column+1); counter++){
            entry = A.getAxEntry(counter);
            row = A.getAiEntry(counter);
            this->setEntry(row,column,entry);
        }
    }
}

void denseMatrix::resize(int rows, int columns)
{
    this->rows = rows;
    this->columns = columns;
    this->values.assign(rows * columns, 0.0);
}


bool denseMatrix::isTransposed()
{
    return transposed;
}

void denseMatrix::setTransposed(bool tr)
{
    transposed=tr;
}

void denseMatrix::transpose()
{

    this->transposed    = !this->transposed ;
    double tmp          = this->columns ;
    this->columns       = this->rows ;
    this->rows          = tmp ;
}


/* Umrechnung der Koordinaten auf Zeilen- und Spaltenzugriffe */
int denseMatrix::ind2Dto1D(int row, int column) const
{
    if ( transposed == true )
        return column * rows + row;
    else
        return row * columns + column;
}


denseMatrix  denseMatrix::operator*(const denseMatrix &m){
    int columns = m.getColumns();
    int rows    = this->getRows() ;
    int inner   = this->getColumns() ;

    if (rows != this->getRows() ){
        std::out_of_range("Die Matrizen passen nicht zueinander!") ;
    }
    double entry ;
    denseMatrix tmp(rows, columns) ;
    //Laufe durch alle Einträge
    LOOP(row, rows ){
        LOOP(column, columns ){
            entry = 0 ;
            // Berechne den jeweiligen Eintrag
            LOOP(i, inner ){
                entry += this->getEntry(row, i) * m.getEntry(i, column) ;
            }
            tmp.setEntry(row, column, entry ) ;
        }
    }
    return tmp ;
}

bool denseMatrix::operator ==(const denseMatrix& m){

    if(rows == m.getRows() && columns == m.getColumns())
    {
        const std::vector<double> * mValues = m.getValues();

        LOOP(index, rows*columns){

            if(!CLOSE(values[index], (*mValues)[index])) return false;
        }
    }
    else return false;
    return true;
}

point denseMatrix::operator*(const point& p)
{
    if (p.getDimension()!=columns)
    { // TODO correct? wollen wir Fehler ungefähr so handhaben?
        std::domain_error e("This matrix-vector multiplication is not valid. ");
        std::cerr << "exception: " << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }

    double temp;
    point pRet(rows);

    LOOP(row, rows){
        LOOP(col, columns){

            temp = getEntry(row,col)*p.getXi(col);
        }
        pRet.setXi(row, temp);
    }

    return pRet;
}



// Overload stream insertion operator out << c (friend)
std::ostream & operator<< (std::ostream & out, const denseMatrix & m)
{
    LOOP(row,m.rows){
        LOOP(column, m.columns){

            out << m.values[m.ind2Dto1D(row, column)] << "\t";
        }

        if(row < m.rows-1) out << std::endl;
    }
    return out;
}


