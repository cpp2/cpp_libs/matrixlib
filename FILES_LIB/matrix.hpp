
/*!
 * \file matrix.hpp
 * \brief Die Basis Matrixklasse
 */
#pragma once

#include <iostream>



/*!
 \brief Die Basis Matrixklasse
 \version 1.0
 \author Lukas Düchting
 \date 2018
 */
class matrix
{
protected:
    int rows; ///< Anzahl Zeilen
    int columns; ///< Anzahl Spalten

public:
    matrix();
    ///Konstruktor für rows x columns Matrix (leer)
    matrix(int rows, int columns);
    ///Getter für die Zeilenanzahl
    int getRows() const;
    ///Getter für die Spaltenanzahl
    int getColumns() const;


//    friend virtual std::ostream & operator<< (std::ostream & out, const matrix & m) = 0;
};



